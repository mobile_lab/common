//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include "autoware_msgs/SteerCmd.h"
#include "autoware_msgs/AccelCmd.h"
#include "autoware_msgs/VehicleCmd.h"

class Converter
{
  public:
  Converter();
  ~Converter();
private:
  float steer = 0, accel = 0;

  ros::NodeHandle n;
  ros::Subscriber steer_sub;
  ros::Subscriber accel_sub;
  ros::Publisher cmd_pub;
  void steerCallback(const autoware_msgs::SteerCmdConstPtr & msg);
  void accelCallback(const autoware_msgs::AccelCmdConstPtr & msg);
  autoware_msgs::VehicleCmd cmd;
  //void convertToCmd();
  int gear = 1;
  

};
Converter::Converter()
{
  steer_sub = n.subscribe("steer_cmd", 1000, &Converter::steerCallback, this);
  accel_sub = n.subscribe("accel_cmd", 1000, &Converter::accelCallback, this);

  cmd_pub = n.advertise<autoware_msgs::VehicleCmd>("vehicle_cmd", 10);
  autoware_msgs::BrakeCmd brake_cmd;
  brake_cmd.brake = 0;
  cmd.brake_cmd = brake_cmd;
  //brake_sub = n.subscribe("brake_cmd", 1000, &IdanSender::brakeCallback, this);
  ros::spin();
}
Converter::~Converter()
{
}
void Converter::steerCallback(const autoware_msgs::SteerCmdConstPtr & msg)
{
  steer =  float(msg->steer)/10000;
  //ROS_INFO_STREAM("steer: "<<steer);
  cmd.header.stamp = ros::Time::now();
  
  //cmd.steer_cmd = *msg;
  // cmd.steer_cmd.steer = 1;
  ROS_INFO_STREAM("steer: "<<cmd.steer_cmd.steer );
  cmd.gear = 3;
  
  cmd.ctrl_cmd.steering_angle = float(msg->steer)/720;

  cmd_pub.publish(cmd);
  //convertToCmd();
}
void Converter::accelCallback(const autoware_msgs::AccelCmdConstPtr & msg)
{
  // accel =  float(msg->accel)/10000;
  // ROS_INFO_STREAM("accel: "<<accel);
  cmd.header.stamp = ros::Time::now();
  //cmd.accel_cmd = *msg;
  cmd.gear = 3;
  ROS_INFO_STREAM("accel: "<<cmd.accel_cmd.accel);
  cmd.ctrl_cmd.linear_acceleration = msg->accel;
  cmd_pub.publish(cmd);
  //convertToCmd();
}
// void IdanSender::brakeCallback(const autoware_msgs::BrakeCmdConstPtr & msg)
// {
//   ROS_INFO_STREAM("brake: "<<msg->brake);
// }
// void Converter::convertToCmd()
// {
    
//     cmd.header.stamp = ros::Time::now();
//     cmd.steering_cmd = steer;
//     cmd.accel_cmd = accel;
    
    
    
	  
// }


 
int main (int argc, char *argv[])

{
  ros::init(argc, argv, "convert_cmd");
  Converter Conv;
  
  
  return 0;
}





