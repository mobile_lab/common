/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// ROS Includes
#include <ros/ros.h>

#include "autoware_msgs/ControlCommandStamped.h"
// User defined includes
//#include "pure_pursuit_core.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "send_commands");
  ros::NodeHandle n;
  
  ros::Publisher pub1 = n.advertise<autoware_msgs::ControlCommandStamped>("ctrl_cmd", 10);

  ROS_INFO_STREAM("send_commands start");
  ros::Rate loop_rate(10);
  while (ros::ok())
  {
    autoware_msgs::ControlCommandStamped ccs;
    ccs.header.stamp = ros::Time::now();
    ccs.cmd.linear_velocity = 0;
    ccs.cmd.linear_acceleration = 70;
    ccs.cmd.steering_angle = 0;

    pub1.publish(ccs);
    ROS_INFO_STREAM("publish now");
    ros::spinOnce();
    loop_rate.sleep();
  }



  return 0;
}
