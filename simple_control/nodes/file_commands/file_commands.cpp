//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include "autoware_msgs/SteerCmd.h"
#include "autoware_msgs/AccelCmd.h"

#include<string>
#include <fstream>






int main (int argc, char *argv[])
{
  //ROS_INFO_STREAM("steer: "<<steer.steer<<"accel: "<< accel.accel);
  ros::init(argc, argv, "file_commands");
  ros::NodeHandle n;
  autoware_msgs::SteerCmd steer;
  autoware_msgs::AccelCmd accel;
  ros::Publisher steer_pub = n.advertise<autoware_msgs::SteerCmd>("steer_cmd", 10);
  ros::Publisher accel_pub = n.advertise<autoware_msgs::AccelCmd>("accel_cmd", 10);

  std::string input_file_name = "/home/gavriel/file_commands_files/slalom_low.txt";
  std::string output_file_name = "/home/gavriel/file_commands_files/slalom_low_out.txt";

  std::ifstream commandFile;
	commandFile.open(input_file_name);
	if (!commandFile.is_open())
	{
		std::cout << "file error\n";
		return true;
	}
  std::cout << "start sending commands from file: " << input_file_name << ".\n";
  std::cout << "continue? [y/n]" << '\n';
  std::string command = { "n" };
  std::cin >> command;
  if (command[0] == 'y')
    std::cout << "start!\n";
  else
  {
    commandFile.close();
    return 0;
  }

  

	int dt;
	float steer_f, accel_f;
	commandFile >> dt;
	if (commandFile.eof())
	{
		std::cout << "empty file\n";
		return true;
	}
  ros::Rate loop_rate(1000./dt);
	while (ros::ok()) 
	{
    commandFile >> steer_f >> accel_f;
    steer.steer = int(steer_f*10000);
    accel.accel = int(accel_f*10000);
    if (commandFile.eof())
      break;
    std::cout << "steer: " << steer.steer << " acc: " << accel.accel << '\n';
    steer_pub.publish(steer);
    accel_pub.publish(accel);

    ros::spinOnce();
    loop_rate.sleep();
		
	}
	commandFile.close();
  
  return 0;
}